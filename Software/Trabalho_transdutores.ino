int Dmin, Dmax, outPin=2, inPin=3, delayInicial = 10, delaymili = 10;
int led0=6, led1=7, led2=8, led3=9;

unsigned long medir(){
  unsigned long t0, t;

  digitalWrite(outPin,LOW);
  while(digitalRead(inPin)==HIGH){}
  delay(1);
  
  t0=micros();
  digitalWrite(outPin, HIGH);
  while(digitalRead(inPin)==LOW){}
  t=micros();
  digitalWrite(outPin, LOW);
  return(t - t0);
}

void printled(int valor){
  if(valor >200){
    digitalWrite(led0,HIGH);
  }else{digitalWrite(led0,LOW);}

    if(valor>400){
    digitalWrite(led1,HIGH);
  }else{digitalWrite(led1,LOW);}

    if(valor>600){
    digitalWrite(led2,HIGH);
  }else{digitalWrite(led2,LOW);}

    if(valor>700){
    digitalWrite(led3,HIGH);
  }else{digitalWrite(led3,LOW);}
  
}

void setup() {
int i;
  pinMode(led0,OUTPUT);
  pinMode(led1,OUTPUT);
  pinMode(led2,OUTPUT);
  pinMode(led3,OUTPUT);

  for(i=0;i<vetNum;i++){
    vetPeso[i]=vetPeso[i]/vetNum;
  }
  
  Serial.begin(115200);
  pinMode(inPin, INPUT);
  pinMode(outPin, OUTPUT);
  digitalWrite(outPin, LOW);

  Serial.println("Medindo valor minimo em:");
 

  for(i=delayInicial; i>0; i--){
    Serial.println(i);
    delay(1000);
  }
  Dmin = medir();
  Serial.print("Valor minimo = ");
  Serial.println(Dmin);
  Serial.println("\n");

 



  Serial.println("Medindo valor maximo em:");
  for(i=delayInicial; i>0; i--){
    Serial.println(i);
    delay(1000);
  }
  Dmax = medir();
  Serial.print("Valor maximo = ");
  Serial.println(Dmax);
  Serial.println("\n");
  
}

void loop() {
  int medida;
  //medida = medir();
  medida= (medir() - Dmin)*(1000/Dmax);

  Serial.println(medida); 
  printled(medida); 
  delay(delaymili);
}
